<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Elastic\Elasticsearch\Client;

class SearchController extends Controller
{
    private $index = 'books';
    
    /**
     * Display results of search by title of books.
     */
    public function searchByTitle(Request $request, Client $client)
    {
        $request->validate([
            'searchQuery' => 'required|string'
        ]);
        
        $searchQuery = $request->searchQuery;
        
        $params = [
            'index' => $this->index,
            'body' => '{
                "query": {
                    "match": {
                        "title": "' . $searchQuery . '"
                    }
                }
            }'
        ];
        
        $response = $client->search($params);
        $books = $response['hits']['hits'];
        $books = json_encode($books);
        
        return $books;
    }

    /**
     * Display results of search by description.
     */
    public function searchByDescription(Request $request, Client $client)
    {
        $request->validate([
            'searchQuery' => 'required|string'
        ]);
        
        $searchQuery = $request->searchQuery;
        
        $params = [
            'index' => $this->index,
            'body' => '{
                "query": {
                    "match": {
                        "description": "' . $searchQuery . '"
                    }
                }
            }'
        ];
        
        $response = $client->search($params);
        $books = $response['hits']['hits'];
        $books = json_encode($books);
        
        return $books;
    }

    /**
     * Display results of search by content.
     */
    public function searchByContent(Request $request, Client $client)
    {
        $request->validate([
            'searchQuery' => 'required|string'
        ]);
        
        $searchQuery = $request->searchQuery;
        
        $params = [
            'index' => $this->index,
            'body' => '{
                "query": {
                    "match": {
                        "content": "' . $searchQuery . '"
                    }
                }
            }'
        ];
        
        $response = $client->search($params);
        $books = $response['hits']['hits'];
        $books = json_encode($books);
        
        return $books;
    }

    /**
     * Display results of search by title, description and content.
     */
    public function searchByTitleDescriptionContent(Request $request, Client $client)
    {
        $request->validate([
            'searchQuery' => 'required|string'
        ]);
        
        $searchQuery = $request->searchQuery;
        
        $params = [
            'index' => $this->index,
            'body' => '{
                "query": {
                    "bool": {
                        "should": [
                            {
                                "match": {
                                    "title": "' . $searchQuery . '"
                                }
                            }, {
                                "match": {
                                    "description": "' . $searchQuery . '"
                                }
                            }, {
                                "match": {
                                    "content": "' . $searchQuery . '"
                                }
                            }
                        ]
                    }
                }
            }'
        ];
        
        $response = $client->search($params);
        $books = $response['hits']['hits'];
        $books = json_encode($books);
        
        return $books;
    }

    /**
     * Display books of an author.
     */
    public function filterByAuthor(Request $request, Client $client)
    {
        $request->validate([
            'author' => 'required|string'
        ]);
        
        $author = $request->author;
        
        if ($author == 'all') {
            $body = '{
                "query": {
                    "match_all": {}
                }                  
            }';
        } else {
            $body = '{
                "query": {
                    "term": {
                        "author": "' . $author . '"
                    }
                }
            }';
        }
        
        $params = [
            'index' => $this->index,
            'body' => $body
        ];
        
        $response = $client->search($params);
        $books = $response['hits']['hits'];
        $books = json_encode($books);
        
        return $books;
    }
}
