<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Elastic\Elasticsearch\Client;

class BookController extends Controller
{
    private $index = 'books';
    
    /**
     * Display a listing of the resource.
     */
    public function index(Client $client)
    {
        $params = [
            'index' => $this->index,
            'body' => '{
                "query": {
                    "match_all": {}
                }
            }'
        ];
        
        $response = $client->search($params);
        $books = $response['hits']['hits'];
        $books = json_encode($books);
        $books = json_decode($books);
        //print_r ($books);
        return view('books.index', ['books' => $books]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('books.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request, Client $client)
    {
        $request->validate([
            'title' => ['required', 'string'],
            'author' => ['required', 'string'],
            'publish_year' => ['required', 'integer', 'digits:4'],
            'description' => ['required', 'string'],
            'content' => ['required', 'string']
        ]);
        
        $params = [
            'index' => $this->index,
            'body' => [
                'title' => $request->title,
                'author' => $request->author,
                'publish_year' => $request->publish_year,
                'description' => $request->description,
                'content' => $request->content
            ]
        ];
        
        $response = $client->index($params);
        
        if ($response->result == 'created') {
            return redirect()->back()->withSuccess('Uspešan unos.');
        } else {
            return redirect()->back()->with('unsuccess', 'Neuspešan unos. Pokušajte ponovo.');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id, Client $client)
    {
        $params = [
            'index' => $this->index,
            'id' => $id
        ];
        
        $response = $client->get($params);
        $book = $response['_source'];
        $book = json_encode($book);
        $book = json_decode($book);
        
        return view('books.show', ['book' => $book]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id, Client $client)
    {
        $params = [
            'index' => $this->index,
            'body' => '{
                "query": {
                    "term": {
                        "_id": "' . $id . '"
                    }
                }
            }'
        ];
        
        $response = $client->search($params);
        $response = $response['hits']['hits'];
        $book = $response[0];
        $book = json_encode($book);
        $book = json_decode($book);
        
        return view('books.edit', ['book' => $book]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id, Client $client)
    {
        $request->validate([
            'title' => ['required', 'string'],
            'author' => ['required', 'string'],
            'publish_year' => ['required', 'integer', 'digits:4'],
            'description' => ['required', 'string'],
            'content' => ['required', 'string']
        ]);
        
        $params = [
            'index' => $this->index,
            'id' => $id,
            'body' => [
                'title' => $request->title,
                'author' => $request->author,
                'publish_year' => $request->publish_year,
                'description' => $request->description,
                'content' => $request->content
            ]
        ];
        
        $response = $client->index($params);
        
        if ($response->result == 'updated') {
            return redirect()->back()->withSuccess('Uspešna izmena');
        } else {
            return redirect()->back()->with('unsuccess', 'Neuspešna izmena. Pokušajte ponovo.');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id, Client $client)
    {
        $params = [
            'index' => $this->index,
            'id' => $id
        ];
        
        $response = $client->delete($params);
        //print_r($response->result);
        
        if ($response->result == 'deleted') {
            return view('books.delete')->with('success', 'Knjiga je izbrisana');
        } else {
            return view('delete')->with('success', 'Knjiga nije izbrisana. Pokušajte ponovo.');
        }
    }
}
