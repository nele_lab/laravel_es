<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Elastic\Elasticsearch\Client;
use Elastic\Elasticsearch\ClientBuilder;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->singleton(Client::class, function() {
            return ClientBuilder::create()
                ->setHosts([config('elasticsearch.auth.host')])
                ->setBasicAuthentication(config('elasticsearch.auth.user'), config('elasticsearch.auth.password'))
                ->setCABundle(config('elasticsearch.auth.cert'))
                ->build();
        });
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
