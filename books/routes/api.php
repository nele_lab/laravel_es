<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\api\SearchController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('searchByTitle', [SearchController::class, 'searchByTitle'])->name('searchByTitle');
Route::post('searchByDescription', [SearchController::class, 'searchByDescription'])->name('searchByDescription');
Route::post('searchByContent', [SearchController::class, 'searchByContent'])->name('searchByContent');
Route::post('searchByTitleDescriptionContent', [SearchController::class, 'searchByTitleDescriptionContent'])->name('searchByTitleDescriptionContent');
Route::post('filterByAuthor', [SearchController::class, 'filterByAuthor'])->name('filterByAuthor');
